package com.mars.module.admin.service;

import com.mars.module.admin.entity.SysDictType;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.SysDictTypeRequest;

import java.util.List;

/**
 * 字典类型接口
 *
 * @author mars
 * @date 2023-11-18
 */
public interface ISysDictTypeService {
    /**
     * 新增
     *
     * @param param param
     * @return SysDictType
     */
    SysDictType add(SysDictTypeRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysDictTypeRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return SysDictType
     */
    SysDictType getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysDictType>
     */
    PageInfo<SysDictType> pageList(SysDictTypeRequest param);

    /**
     * 获取所有
     *
     * @return List<SysDictType>
     */
    List<SysDictType> list();

}
