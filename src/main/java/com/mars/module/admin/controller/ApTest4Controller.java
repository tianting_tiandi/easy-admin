package com.mars.module.admin.controller;


import java.util.Arrays;
import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.ApTest4;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApTest4Service;
import com.mars.module.admin.request.ApTest4Request;

/**
 * 测试4控制层
 *
 * @author mars
 * @date 2023-12-22
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "测试4接口管理",tags = "测试4接口管理")
@RequestMapping("/admin/apTest4" )
public class ApTest4Controller {

    private final IApTest4Service iApTest4Service;

    /**
     * 分页查询测试4列表
     */
    @ApiOperation(value = "分页查询测试4列表")
    @PostMapping("/pageList")
    public R<PageInfo<ApTest4>> pageList(@RequestBody ApTest4Request apTest4) {
        return R.success(iApTest4Service.pageList(apTest4));
    }

    /**
     * 获取测试4详细信息
     */
    @ApiOperation(value = "获取测试4详细信息")
    @GetMapping(value = "/query/{id}")
    public R<ApTest4> detail(@PathVariable("id") Long id) {
        return R.success(iApTest4Service.getById(id));
    }

    /**
     * 新增测试4
     */
    @Log(title = "新增测试4", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增测试4")
    @PostMapping("/add")
    public R<Void> add(@RequestBody ApTest4Request apTest4) {
        iApTest4Service.add(apTest4);
        return R.success();
    }

    /**
     * 修改测试4
     */
    @Log(title = "修改测试4", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改测试4")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody ApTest4Request apTest4) {
        iApTest4Service.update(apTest4);
        return R.success();
    }

    /**
     * 删除测试4
     */
    @Log(title = "删除测试4", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除测试4")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iApTest4Service.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
