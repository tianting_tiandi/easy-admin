package com.mars.module.tool.mapper;


import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.tool.entity.GenTableColumn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 业务字段 数据层
 *
 * @author mars
 */
public interface GenTableColumnMapper extends BasePlusMapper<GenTableColumn> {
    /**
     * 根据表名称查询列信息
     *
     * @param tableName 表名称
     * @return 列信息
     */
    List<GenTableColumn> selectDbTableColumnsByName(@Param("tableName") String tableName);

    /**
     * 查询业务字段列表
     *
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
    List<GenTableColumn> selectGenTableColumnListByTableId(@Param("tableId") Long tableId);

}
