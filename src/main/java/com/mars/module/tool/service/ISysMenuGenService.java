package com.mars.module.tool.service;

import com.mars.module.tool.entity.GenSysMenu;
import com.mars.module.tool.request.GenSysMenuRequest;

import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-08 17:14:07
 */
public interface ISysMenuGenService {

    /**
     * 保存菜单
     *
     * @param request   request
     * @param request   functionName
     * @param tableName tableName
     */
    void saveGenMenu(GenSysMenuRequest request, String functionName, String tableName);

    /**
     * 获取最大的排序
     *
     * @param type 1 目录 2 菜单
     * @return Integer
     */
    Integer selectMaxSort(Integer type, Long menuId);

    /**
     * 根据名称 子级菜单id 列表
     *
     * @param type     1 目录 2 菜单
     * @param menuName 菜单名称
     * @return List<Long>
     */
    List<Long> selectListByMenuName(Integer type, String menuName);

    /**
     * 根据菜单名称查询菜单
     *
     * @param menuName 菜单名称
     * @return GenSysMenu
     */
    GenSysMenu selectMenuByName(String menuName);
}
