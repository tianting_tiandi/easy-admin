package com.mars.module.system.mapper;

import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.SysRoleMenu;

import java.util.List;

/**
 * 角色关联菜单
 *
 * @author 源码字节-程序员Mars
 */
public interface SysRoleMenuMapper extends BasePlusMapper<SysRoleMenu> {


    /**
     * 根据roleId查询
     *
     * @param roleId 角色id
     * @return List<Long>
     */
    List<Long> selectByRoleId(Long roleId);

    /**
     * 根据roleId删除
     *
     * @param roleId 角色id
     */
    void deleteByRoleId(Long roleId);

}
