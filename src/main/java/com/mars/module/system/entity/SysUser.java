package com.mars.module.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

/**
 * 用户
 *
 * @author 源码字节-程序员Mars
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUser extends BaseEntity {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 姓名
     */
    private String realName;

    /**
     * 性别(1男  2女)
     */
    private Integer sex;

    /**
     * 出生日期
     */
    private LocalDate birthDate;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户类型(0内置用户 1注册用户)
     */
    private Integer type;

    /**
     * 头像
     */
    private String avatar;


    /**
     * 地址
     */
    private String address;


}
