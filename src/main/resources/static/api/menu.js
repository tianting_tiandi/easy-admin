/**
 * 菜单分页列表
 * @param query
 * @returns {*}
 */
function pageList(query) {
    return requests({
        url: '/sys/menu/list',
        method: 'post',
        data: query
    })
}

/**
 * 菜单详情
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/sys/menu/get/' + id,
        method: 'get'
    })
}
